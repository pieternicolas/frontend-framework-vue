// [dependencies]

var path    = require ( 'path' );
var webpack = require ( 'webpack' );


// [init] Optimisers.

new webpack.optimize.CommonsChunkPlugin ( 'common.js' );
new webpack.optimize.DedupePlugin ();
new webpack.optimize.UglifyJsPlugin ();
new webpack.optimize.AggressiveMergingPlugin ();


const ExtractTextPlugin = require ( 'extract-text-webpack-plugin' );


// [build] Configuration.

module.exports = {
	
	entry: './src/restricted/main.js',
	
	output: {
		
		path: path.resolve ( __dirname, './dist' ),
		publicPath: '/dist/',
		filename: 'build.js'
		
	},
	
	module: {
		
		rules: [
			
			
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					
					loaders: {
						
						'scss': 'vue-style-loader!css-loader!sass-loader',
						'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
						
					}
					
				}
			},
			
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract ( {
					
					fallback: "style-loader",
					use: "css-loader"
					
				} )
			},
			
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				options: {
					presets: ['env'],
					plugins: ['babel-plugin-root-import']
				}
			},
			
			{
				test: /\.(png|jpg|gif|svg)$/,
				loader: 'file-loader',
				options: {
					
					name: '[name].[ext]?[hash]'
					
				}
			},

			{
				test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
				loader: 'url-loader',
				options: {
					limit: 10000,
					name: 'fonts/[name].[hash:7].[ext]'
				}
			}
			
		]
		
	},
	
	resolve: {
		
		alias: {
			
			'vue$': 'vue/dist/vue.esm.js'
			
		}
		
	},
	
	devServer: {
		
		historyApiFallback: true,
		noInfo: true
		
	},
	
	performance: {
		
		hints: false
		
	},
	
	devtool: '#eval-source-map',
	
	plugins: []
	
	
};

if ( process.env.NODE_ENV === 'production' ) {
	
	module.exports.devtool = '#source-map'
	
	module.exports.plugins = ( module.exports.plugins || [] ).concat ( [
	
		new webpack.DefinePlugin ( {
			
			'process.env': {
				
				NODE_ENV    : '"production"',
				ENDPOINT    : '"https://elasticsearch.digitalfolks.sg/digitalfolks"',
				SIGNIN_API  : '"/users/signin"',
				USERS_API   : '"/users"',
				METHODTRACE : '"off"'
				
			}
			
		} ),
		
		new webpack.optimize.UglifyJsPlugin ( {
			
			sourceMap: true,
			compress: {
				
				warnings: false
				
			}
			
		} ),
		
		new webpack.LoaderOptionsPlugin ( {
			
			minimize: true
			
		} )
	
	] );
	
}
else {
	
	module.exports.plugins = ( module.exports.plugins || [] ).concat ( [
	
		new webpack.DefinePlugin ( {
			
			'process.env': {
				
				NODE_ENV    : '"development"',
				ENDPOINT    : '"https://elasticsearch.digitalfolks.sg/digitalfolks"',
				SIGNIN_API  : '"/users/signin"',
				USERS_API   : '"/users"',
				METHODTRACE : '"on"'
				
			}
			
		} )
	
	] );
	
}
