// [init]

export default {
	
	
	namespaced: true,
	
	state: {
		
		foo: 'bar'
		
	},
	
	mutations: {
		
		updateFoo ( state, newFoo ) {
			
			state.foo = newFoo;
			
		}
		
	},
	
	actions: {
		
		getFooFromServer ( { commit, state }, someParams ) {
			
			return new Promise ( ( resolve, reject ) => {
				
				Axios.request ( {
						
					method         : 'post',
					url            : '/api/fetchFoo',
					validateStatus : function ( status ) { return status == 200; },
					data           : {
						
						a : someParams.a,
						b : someParams.b
						
					}
					
				} )
				.then ( ( response ) =>  {
					
					var results = response.data.results;
					
					commit ( 'updateFoo', results );
					
					resolve ( response );


				} )
				.catch ( ( error ) => reject () );
				
			} );
			
		}
		
	}
	
}