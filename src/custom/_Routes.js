export default {

	routes: [
	
		{ path: '/dashboard', alias: '/', component: resolve => require ( ['./views/dashboard.vue'], resolve ) },
		{ path: '/signin', component: resolve => require ( ['./views/signin.vue'], resolve ) },
		{ path: '/profile', component: resolve => require ( ['./views/profile.vue'], resolve ) },
		{ path: '/todos', component: resolve => require ( ['./views/todos.vue'], resolve ) },
		{ path: '/todos/new', component: resolve => require ( ['./views/todos-form.vue'], resolve ) },
		{ path: '/todos/:todoID/edit', component: resolve => require ( ['./views/todos-form.vue'], resolve ) },
		{ path: '/todos/:todoID', component: resolve => require ( ['./views/todo.vue'], resolve ) }
		
	]

}