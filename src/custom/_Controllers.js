// [controllers] Include custom controllers here.

import Todos from './controllers/Todos.js';


// [init]

export default {
	
	modules: {
		
		
		// Create custom controller references here.
		
		todos : Todos
		
		
	}
	
}