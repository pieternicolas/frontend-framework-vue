// [init]

export default {
			
	namespaced: true,
	
	state: {
	
		
		routes : {
		
		
			// List of routes that both public and members can access.
			// Any route not declared here is considered private.
			// DO NOT REMOVE THIS PROPERTY.
		
			anyUserRoutes: [
				
				'/privacy-policy',
				'/terms-of-service'
				
			],
			
			
			// This default route to go to if the user is not signed-in and attempts to access a private route.
			// DO NOT REMOVE THIS PROPERTY.
			
			defaultPublicRoute: '/signin',
			
			
			// List of routes that only public can access.
			// If a signed-in user attempts to access these pages, he/she will be redirect to `defaultSignedInRoute`.
			// DO NOT REMOVE THIS PROPERTY.
			
			publicOnlyRoutes: [
				
				'/signin',
				'/forgot-password',
				'/reset-password'
				
			],
			
			
			// The default route to go to if the user is signed-in but attempted to access a `publicOnlyRoute`.
			// DO NOT REMOVE THIS PROPERTY.
			
			defaultSignedInRoute: '/',
			
			
			// The default route to go to when the user has logged out.
			
			defaultSignedOutRoute: '/signin'
		
		
		}

	
	}
	
}