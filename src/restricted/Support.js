// [init]

export default {
	
	
	namespaced: true,
	
	state: {
		
		
		// Indicates if the current screen’s form is not yet saved.
		
		unsaved: false
		
		
	},
	
	mutations: {
		
		setSaved ( state, saved ) {
			
			
			state.unsaved = saved === false ? true : false;
			
			
		}
		
	},
	
	actions: {}
	
}