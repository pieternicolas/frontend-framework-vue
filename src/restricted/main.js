// ! [dependencies] Import all dependencies.

	// Babel

	import 'babel-polyfill';


	// jQuery and Bootstrap.
	
	import jQuery from 'jquery';
	
	global.jQuery = jQuery;
	global.$      = jQuery;
	
	var Bootstrap = require ( 'bootstrap' );
	
	
	// VueJS related.

	import Vue       from 'vue';
	import Vuex      from 'vuex';
	import VueRouter from 'vue-router';
	
	
	// Configure Axios as a global variable.
	
	import Axios from 'axios';
	
	global.axios = Axios;
	
	
	// Application related.
	
	import App          from './App.js';
	import _App         from '../custom/_App.vue';
	import _Controllers from '../custom/_Controllers.js';
	import _Routes      from '../custom/_Routes.js';
	

// ! [dependencies] Initialize all dependencies.

Vue.use ( Vuex );
Vue.use ( VueRouter );


// ! [axios] Configure axios.
	
Axios.defaults.baseURL = process.env.ENDPOINT;


// ! [route] Configure routing.

const router = new VueRouter ( { routes: _Routes.routes } );


// ! [vuex] Configure the Vuex store.


	// Pull restricted controllers.

	import Auth from './Auth.js';
	import Support from './Support.js';
	
	_Controllers.modules.auth = Auth;
	_Controllers.modules.support = Support;


	// Pull custom constrollers.
	
	import _Access   from '../custom/_Access.js';

	_Controllers.modules.access = _Access;


	// Initialize the store.
	
	const store = new Vuex.Store ( _Controllers );
	

// ! [execute] Start Vue application.

new Vue ( {
	
	router,
	store,
	
	el: '#app',
	
	mixins: [App],
	
	render: h => h ( _App )
	
} );
