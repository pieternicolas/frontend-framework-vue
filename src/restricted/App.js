
export default {
	
	
	// ! component.name
	
	name: 'app',
	
	
	// ! component.data
				
	data () { return {} },
	
	
	// ! component.created
		
	created () { this.methodTrace ( '[methodtrace] root.created' );
		
		
		// [auth] Check if user has an active session.
		
		this.$store.dispatch ( 'auth/authenticate', false, { root: true } );

		
		// [events] Setup routing guards/hooks.
		
		this.$router.beforeEach ( this.beforeRoute );
		
		this.$router.afterEach ( this.afterRoute );


	},
	
	
	// ! component.mounted
	
	mounted () { this.methodTrace ( '[methodtrace] root.mounted' );
		
	}, // END: Vue.mounted
		
	methods: {
		
		
		// Perform a method trace log. (STABLE)
		
		methodTrace () {
			
			if ( process.env.METHODTRACE === 'on' ) console.log.apply ( console, arguments );
			
		},
		
		
		// Execute before going to a route.
		
		beforeRoute ( to, from, next ) { this.methodTrace ( '[methodtrace] root.beforeRoute: ' + to.path );
			
			
			// There are unsaved changes.
			
			if ( this.$store.state.support.unsaved === true ) {
				
				
				// User is ok with not saving.
				
				if ( confirm ( 'You have unsaved changes, continue?' ) ) {
					
					
					// Update `unsaved` variable.
					
					this.$store.commit ( 'support/setSaved', true );
					
					
				}
				
				
				// User wants to save.
				
				else {
					
					
					// Cancel routing.
					
					next ( false );
					
					
					// No need to do any more checks.
					
					return;
					
					
				}
				
				
			}
			
			
			// Controls.
			
			var allowAccess = false;
			
			
			// User is NOT signed-in.
			
			if ( this.$store.state.auth.isSignedIn !== true ) {
				
				
				// User is accessing a public-only route.
				
				if ( this.$store.state.access.routes.publicOnlyRoutes.indexOf ( to.path ) !== -1 ) allowAccess = true;
				
				
				// User is accessing a neutral route.
				
				else if ( this.$store.state.access.routes.anyUserRoutes.indexOf ( to.path ) !== -1 ) allowAccess = true;
				
				
			}
			
			// User is signed-in.
			
			else {
				
				
				// User is not accessing a public-only route.
				
				if ( this.$store.state.access.routes.publicOnlyRoutes.indexOf ( to.path ) === -1 ) allowAccess = true;
	
	
			}
			
			
			// User is not allowed.
			
			if ( allowAccess !== true ) {
				
				
				// User is not signed-in.
				
				if ( this.$store.state.auth.isSignedIn !== true ) {
					
					
					// Cancel routing.
					
					next ( false );
					
					
					// Sign the user out.
					
					this.signout ();
					
				}
				
				
				// User is signed-in; route to the default signed-in page.
				
				else next ( this.$store.state.access.routes.defaultSignedInRoute );
				
				
			}
			else next ();
			
			
		},
		
		
		// Execute after going to a route. (STABLE)

		afterRoute ( to, from ) { this.methodTrace ( '[methodtrace] root.afterRoute: ' + to.path );
			
				
			// Run the `startView` method on the new view.
			
			setTimeout ( this.startView, 100 );
			
			
		},
		
		
		// Attempt to launch the start method of the current view. (STABLE)
		
		startView () {
			
			
			try {
				
				this.$children[0].$refs.view.start ();
				
				this.methodTrace ( '[methodtrace] root.startView (successful)' );
				
			} catch ( e ) {}


		},
		
		
		// Sign-in the user. (STABLE)
		
		signin ( params ) {
			
			
			return new Promise ( ( resolve, reject ) => {


				// Validate.
			
				if ( ! params.identity || ! params.password ) { reject ( 'Please fill up all fields!' ); return; }
				
				params.identity = params.identity.trim ();
				params.password = params.password.trim ();
				
				if ( params.identity == '' || params.password == '' ) { reject ( 'Please fill up all fields!' ); return; }


				// Send to server for verification
				
				this.$store.dispatch ( 'auth/signin', params )
				.then ( ( response ) => {
			
			
					// Route to the signed-in page.
					
					this.$router.push ( this.$store.state.access.routes.defaultSignedInRoute );
			
					
				} )
				.catch ( ( error ) => reject ( error ) );


			} );
			
			
		},
		
		
		// Sign the user out. (STABLE)
		
		signout () { this.methodTrace ( '[methodtrace] root.signout' );
				
				
			// Destroy all user-related data.
			
			this.$store.dispatch ( 'auth/signout', false, { root: true } );
			
			
			// Route to the signed-out page.
			
			this.$router.push ( this.$store.state.access.routes.defaultSignedOutRoute );
					
					
		}
		
		
	} // END: Vue.methods

	
}