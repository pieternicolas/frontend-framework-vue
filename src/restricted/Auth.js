// [init]

export default {


	namespaced: true,
	
	state: {
		
		
		// Determines if the user is signed-in or not.
		
		isSignedIn : true, 
		
		
		// The JWT received from the server post sign-in.
		
		jwt        : '', 
		
		
		// The user’s ID.
		
		id         : '', 
		
		
		// Other user details.
		
		firstname  : '', 
		lastname   : '',
		email      : ''
	
	
	},
	
	mutations: {
		
		
		// Manipulate user’s data. (STABLE)

		storeUserData ( state, params ) {
			
			
			// Store all user properties.
			
			for ( var key in params ) state[key] = params[key];
			
			
			// Set user as signed-in.
			
			state.isSignedIn = true;
			
			
			// Instruct Axios to add the `Authorization` header for all requests from this point.
			
			global.axios.defaults.headers.common['Authorization'] = 'Bearer ' + state.jwt;
			

			// Store JWT in localStorage/sessionStorage.
			
			if ( params.staySignedIn ) localStorage.setItem ( 'params', JSON.stringify ( state ) );
			else sessionStorage.setItem ( 'params', JSON.stringify ( state ) );
			

		},
	
		removeUserData ( state ) {
			
			
			// Remove all user properties.
			
			for ( var key in state ) state[key] = '';
			
			
			// Set user as signed-out.
			
			state.isSignedIn = false;
			
			
			// Instruct Axios to NO LONGER add the `Authorization` header for all requests from this point.
			
			delete global.axios.defaults.headers.common['Authorization'];
			
			
			// Remove localStorage/sessionStorage property.
			
			try {
				
				sessionStorage.removeItem ( 'params' );
				localStorage.removeItem ( 'params' );
				
			}
			catch ( e ) {}


		}
		
	
	}, // END: mutations
	
	actions: {
		
		
		// Authentication actions. (STABLE)

		signin ( { commit, state }, params ) {


			// Validate and send the request.
			
			return new Promise ( ( resolve, reject ) => {
				
				
				// Validate.
				
				params.identity = params.identity.trim ();
				params.password = params.password.trim ();
				
				if ( ! params.identity || ! params.password ) reject ( '[error] Missing `identity` and/or `password` parameter.' );
				
				
				// Make the request.
				
				global.axios.request ( {
						
					method         : 'post',
					url            : process.env.SIGNIN_API,
					validateStatus : function ( status ) { return status == 200; },
					data           : {
						
						identity : params.identity,
						password : params.password
						
					}
					
				} )
				
				
				// Success.
				
				.then ( ( response ) =>  {
					
					
					// Controls.
					
					var userData = response.data;
					
					userData.staySignedIn = params.staySignedIn;
					
					
					// Store in the state.
					
					commit ( 'storeUserData', userData );
					
					
					// Resolve.
					
					resolve ( response );


				} )
				
				
				// Error.
				
				.catch ( ( error ) => reject ( error ) );
				
				
			} );


		},
				
		signout ( { commit, state }, params ) {


			// Sign the user out completely.
			
			commit ( 'auth/removeUserData', false, { root: true } );	


		},
		
		authenticate ( { commit } ) {
			
			
			// Retrieve stored user data.
			
			var localRecord = sessionStorage.getItem ( 'params' ) ? sessionStorage.getItem ( 'params' ) : localStorage.getItem ( 'params' );
			

			// User does not have a session.
			
			if ( ! localRecord ) return commit ( 'removeUserData' );
			
			
			// Record is a proper JSON string.
			
			try {
				
				commit ( 'storeUserData', JSON.parse ( localRecord ) );
				
			}
			catch ( error ) {
				
				commit ( 'removeUserData' );
				
			}


		},
		
		fetch ( { commit, state }, params ) {
			
			
			return new Promise ( ( resolve, reject ) => {
				
				global.axios.request ( {
						
					method         : 'get',
					url            : process.env.USERS_API + '/' + state.id,
					validateStatus : function ( status ) { return status == 200; }
					
				} )
				.then ( ( response ) => resolve ( response.data ) )
				.catch ( ( error ) => reject ( error ) );
				
			} );
			
			
		},		

		update ( { commit, state }, params ) {
			
			return new Promise ( ( resolve, reject ) => {
				
				global.axios.request ( {
						
					method         : 'post',
					url            : process.env.USERS_API + '/' + state.id + '/update',
					validateStatus : function ( status ) { return status == 200; },
					data           : params
					
				} )
				.then ( ( response ) =>  {
					
					
					// If we are updating the current user’s details, then update the state as well.
					
					commit ( 'storeUserData', params );
					
					
					// All’s good!
					
					resolve ( response );
					
					
				} )
				.catch ( ( error ) => reject ( error ) );
				
			} );
			
		}

	
	} // END: actions


}