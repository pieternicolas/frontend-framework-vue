### Objectives

This documentation aims to provide clear instructions on:

1. The technologies used in this project.
1. How files are organised in this project.
1. How to create and use controllers, components, stylesheets and views.

<br>

---

### Continuous learning

The following technologies are used in this project. Developers should continuously study these in order of priority listed.

1. [VueJS](http://vuejs.org) - This is the main JavaScript framework used..
1. [Vuex](http://vuex.vuejs.org/en/) - This is a state-management plugin for VueJS that provides best-practices for registering and calling global variables.
1. [Axios](https://github.com/mzabriskie/axios) - This is the promise-based HTTP client used to call any API.
1. [Bootstrap](http://getbootstrap.com) - We are currently using version 3.3.7.
1. [Webpack](http://webpack.github.io/) - This is the package manager used for this project.
1. [Vue Loader](http://vuejs.github.io/vue-loader) - This is the webpack loader that enables .vue files.
1. [Firebase](http://firebase.google.com) - This is the real-time database engine we will be using.

In addition to the above, optimum deployment of this project requires additional study in HTML5 and CSS3.

<br>

---

### Installation

```
npm install -g vue-cli
mkdir projectName
cd projectName
vue init webpack-simple
npm install
npm install --save vue-router
npm install --save vuex
npm install --save axios
npm install --save-dev extract-text-webpack-plugin
npm install --save jquery
npm install --save bootstrap
npm run dev
npm run build
```

<br>

---

### File organisation

**/index.html**  

1. Change the contents of the `<title>` tag if needed.
1. Do not change anything else.

**/src/custom/_Access.js**  

1. Holds global access instructions for the router.
1. Do not remove the following properties: `anyUserRoutes`, `defaultPublicRoute`, `publicOnlyRoutes`, `defaultSignedInRoute`.
1. Details about what does properties do is documented in the file itself.

**/src/custom/_App.vue**  

1. This is the top-level template file for the entire application.
1. Update the contents of the `<template>` tag to make changes to the HTML structure of the application at the highest level.
1. Do not change the **id** property of the top level `<div>` tag; it should be “app”.
1. Update the contents of the `<style lang="scss">` to add additional SCSS files to be imported.
1. Add custom components for the scope of this template in the `<script>` tag.
1. Do not change the **ref** property of the `<router-view>` tag; it should be “view”.

**/src/custom/_Controllers.js**  

1. Register controllers.

**/src/custom/_Routes.js**  

1. Register routes.

**/src/custom/components**  

1. Holds all components named in the format `{components}.vue`.

**/src/custom/controllers**  

1. Holds all controllers named in the format `{controller}.js`.

**/src/custom/stylesheets**  

1. Add new global stylesheets here; scoped-level styles should be declared in their respective _View_ files.
1. See the section of ‘Stylesheets’ below for more information.

**/src/custom/views**  

1. Holds all router views named in the format `{route}.vue`.

**/src/restricted**  

1. Do not modify any files in here.

<br>

---

### Components

Components provide reusable and isolated functionality for the application. Headers, footers, sidebars, navigation menus, form sections etc are examples.

1. Create a copy of **/src/custom/components/_Template.vue**, e.g, sample.vue in the **/src/custom/components** directory.
1. Import and enable our component in any _View_ or the top-level **/src/custom/_App.vue** file.

<br>

---

### Controllers

Controllers do the following:

1. Handle HTTP requests.
1. Store data at the global level.

Here's how you create new controllers.

1. Create a copy of **/src/custom/controllers/_Template.js**, e.g, my-controller.js in the **/src/custom/controllers** directory.
1. Register the controller in **/src/custom/_Controllers.js**.

---

### Views and Routing

Views are essentially screens that the user will see based on the URL being accessed.

1. Create a copy of **/src/custom/views/_Template.vue**, e.g, courses.vue in the **/src/custom/views** directory.
1. Change the words “ENTER\_MODULE\_NAME_HERE” in the `<script>` section of the file to the name of the view (usually the same name as the file).=
1. Create the HTML code for this view in the `<template>` section of the file; and
1. Create CSS styles that are specific for this _View_ in the `<style>` section of the file.
1. If you require the user of VueJS, work within the `<script>` section of the file.
1. If you need to use a component within a _View_, you first import it. The template file has an example called “sample”.

To create a route to a view, create the _View_ file (see above) then add the following entry and replace `{view}` with the name of the _View_ file.

```
{
  path: '/{view}', 
  component: resolve => require ( ['./views/{view}.vue'], resolve )
}
```

Aliases are not necessary but if you want to use them, the following snippet can be used.

```
{
  path: '/{view}', 
  alias: '/my-alias', 
  component: resolve => require ( ['./views/{view}.vue'], resolve )
}
```

_Views_ are loaded into the `<router-view>` element found in **/src/_App.vue**.

<br>

---

### Global variables

Access the following global (immutable) variables by using `this.$store.state.{{variable}}`.

| Variable        | Description |
|-----------------|-------------|
| auth.isSignedIn | `boolean` Indicates if the user is signed-in (default false). |
| auth.id         | `string` The ID of the currently logged in user (or ''). |
| auth.firstname  | `string` The current user’s first name (or ''). |
| auth.lastname   | `string` The current user’s last name (or ''). |
| auth.email      | `string` The currrent user’s email address (or ''). |

To create custom configurations that you want to use globally, edit the **/src/controllers/Config.js** file and access the variable through `this.$store.state.config.{{variable}}`.

<br>

---

### Dealing with unsaved forms

The boolean variable at `this.$store.state.support.unsaved` tells the application if there are unsaved changes to a form that the user is currently filling out. 
You can tell the application that the user has not saved the current form by using `this.$store.commit ( 'support/setSaved', false )`.
A confirmation dialog window saying “You have unsaved changes, continue?” will appear if the user attempts to navigate away from this page.

<br>

---


